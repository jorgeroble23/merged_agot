# 1158  - Xunkai

# County Title
title = c_xunkai

# Settlements
max_settlements = 4

b_xunkai_castle = castle
b_xunkai_city1 = city
b_xunkai_temple = temple
b_xunkai_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_xunkai_castle = ca_asoiaf_yiti_basevalue_1
	b_xunkai_castle = ca_asoiaf_yiti_basevalue_2

	b_xunkai_city1 = ct_asoiaf_yiti_basevalue_1
	b_xunkai_city1 = ct_asoiaf_yiti_basevalue_2

}
	