# # 1331 - Rhaenys

# County Title
title = c_rhaenys

# Settlements
max_settlements = 3
b_rhaenys_1 = castle
b_rhaenys_2 = city

# Misc
culture = summer_islander
religion = summer_rel

terrain = jungle

#History
1.1.1 = {
	b_rhaenys_1 = ca_asoiaf_summerisland_basevalue_1
	b_rhaenys_2 = ct_asoiaf_summerisland_basevalue_1
}