# 1189  - Banxue

# County Title
title = c_banxue

# Settlements
max_settlements = 4

b_banxue_castle = castle
b_banxue_city1 = city
b_banxue_temple = temple
b_banxue_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_banxue_castle = ca_asoiaf_yiti_basevalue_1
	b_banxue_castle = ca_asoiaf_yiti_basevalue_2

	b_banxue_city1 = ct_asoiaf_yiti_basevalue_1
	b_banxue_city1 = ct_asoiaf_yiti_basevalue_2

}
	