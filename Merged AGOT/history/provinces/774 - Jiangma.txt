# 774  - Jiangma

# County Title
title = c_jiangma

# Settlements
max_settlements = 6

b_jiangma_castle1 = castle
b_jiangma_city1 = city
b_jiangma_temple = temple
b_jiangma_city2 = city
b_jiangma_castle2 = city
b_jiangma_castle3 = castle

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_jiangma_castle1 = ca_asoiaf_yiti_basevalue_1
	b_jiangma_castle1 = ca_asoiaf_yiti_basevalue_2
	b_jiangma_castle1 = ca_asoiaf_yiti_basevalue_3

	b_jiangma_city1 = ct_asoiaf_yiti_basevalue_1
	b_jiangma_city1 = ct_asoiaf_yiti_basevalue_2

	b_jiangma_city2 = ct_asoiaf_yiti_basevalue_1
	b_jiangma_city2 = ct_asoiaf_yiti_basevalue_2

}
	