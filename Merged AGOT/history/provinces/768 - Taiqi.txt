# 768  - Taiqi

# County Title
title = c_taiqi

# Settlements
max_settlements = 6

b_taiqi_castle1 = castle
b_taiqi_city1 = city
b_taiqi_temple = temple
b_taiqi_city2 = city
b_taiqi_castle2 = city
b_taiqi_castle3 = castle

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_taiqi_castle1 = ca_asoiaf_yiti_basevalue_1
	b_taiqi_castle1 = ca_asoiaf_yiti_basevalue_2
	b_taiqi_castle1 = ca_asoiaf_yiti_basevalue_3

	b_taiqi_city1 = ct_asoiaf_yiti_basevalue_1
	b_taiqi_city1 = ct_asoiaf_yiti_basevalue_2
	b_taiqi_city1 = ct_asoiaf_yiti_basevalue_3

	b_taiqi_city2 = ct_asoiaf_yiti_basevalue_1
	b_taiqi_city2 = ct_asoiaf_yiti_basevalue_2
	b_taiqi_city2 = ct_asoiaf_yiti_basevalue_3

}
	