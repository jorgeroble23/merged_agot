# 1157  - Pingluo

# County Title
title = c_pingluo

# Settlements
max_settlements = 4

b_pingluo_castle = castle
b_pingluo_city1 = city
b_pingluo_temple = temple
b_pingluo_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_pingluo_castle = ca_asoiaf_yiti_basevalue_1
	b_pingluo_castle = ca_asoiaf_yiti_basevalue_2

	b_pingluo_city1 = ct_asoiaf_yiti_basevalue_1
	b_pingluo_city1 = ct_asoiaf_yiti_basevalue_2

}
	