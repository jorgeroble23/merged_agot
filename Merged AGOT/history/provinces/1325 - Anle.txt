# 1228  - Anle

# County Title
title = c_anle

# Settlements
max_settlements = 4

b_anle_castle = castle
b_anle_city1 = city
b_anle_temple = temple
b_anle_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_anle_castle = ca_asoiaf_yiti_basevalue_1
	b_anle_castle = ca_asoiaf_yiti_basevalue_2

	b_anle_city1 = ct_asoiaf_yiti_basevalue_1
	b_anle_city1 = ct_asoiaf_yiti_basevalue_2

}
	