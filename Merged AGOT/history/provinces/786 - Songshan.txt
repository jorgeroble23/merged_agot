# 786  - Songshan

# County Title
title = c_songshan

# Settlements
max_settlements = 6

b_songshan_castle1 = castle
b_songshan_city1 = city
b_songshan_temple = temple
b_songshan_city2 = city
b_songshan_castle2 = city
b_songshan_castle3 = castle

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_songshan_castle1 = ca_asoiaf_yiti_basevalue_1
	b_songshan_castle1 = ca_asoiaf_yiti_basevalue_2
	b_songshan_castle1 = ca_asoiaf_yiti_basevalue_3

	b_songshan_city1 = ct_asoiaf_yiti_basevalue_1
	b_songshan_city1 = ct_asoiaf_yiti_basevalue_2
	b_songshan_city1 = ct_asoiaf_yiti_basevalue_3

	b_songshan_city2 = ct_asoiaf_yiti_basevalue_1
	b_songshan_city2 = ct_asoiaf_yiti_basevalue_2
	b_songshan_city2 = ct_asoiaf_yiti_basevalue_3

}
	