# 1152  - Dongping

# County Title
title = c_dongping

# Settlements
max_settlements = 4

b_dongping_castle = castle
b_dongping_city1 = city
b_dongping_temple = temple
b_dongping_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_dongping_castle = ca_asoiaf_yiti_basevalue_1
	b_dongping_castle = ca_asoiaf_yiti_basevalue_2

	b_dongping_city1 = ct_asoiaf_yiti_basevalue_1
	b_dongping_city1 = ct_asoiaf_yiti_basevalue_2

}
	