namespace = winter_soldier

#winter_soldier.1 Add Northern Soldier trait to those who live in the North, at The Wall, and beyond
character_event = {
	id = winter_soldier.1
	hide_window = yes
	
	trigger = {
		AND = {
			NOR = {
				trait = white_walker
				culture_group = winter_group
				trait = dragon
				culture = dragon_culture
				graphical_culture = dragongfx
			}
			is_adult = yes
			martial = 8
			capital_scope = {
				OR = {
					region = world_beyond_the_wall
					region = world_the_wall
					region = world_north
				}
			}
		}
	}
	
	immediate = {
		add_trait = northern_winter_soldier
	}
}

#winter_soldier.2 Add Winter Soldier trait to White Walkers
character_event = {
	id = winter_soldier.2
	hide_window = yes
	
	trigger = {
		trait = white_walker
	}
	
	immediate = {
		add_trait = white_walker_winter_soldier
	}
}

#winter_soldier.3 Counteract mild winter effects
province_event = {
	id = winter_soldier.3
	hide_window = yes
	
	trigger = {
		is_winter_trigger = yes
		OR = {
			region = world_beyond_the_wall
			region = world_the_wall
			region = world_north
		}
		owner = {
			NOR = {
				trait = white_walker
				culture = winter_wasteland
			}
		}
		has_province_modifier = asoiaf_mild_winter
	}
	
	mean_time_to_happen = {
		days = 7
	}
	
	option = {
		name = "Ok"
		if = {
			limit = {
				has_province_modifier = white_walker_mild_winter
			}
			remove_province_modifier = white_walker_mild_winter
		}
		if = {
			limit = {
				has_province_modifier = white_walker_normal_winter
			}
			remove_province_modifier = white_walker_normal_winter
		}
		if = {
			limit = {
				has_province_modifier = white_walker_harsh_winter
			}
			remove_province_modifier = white_walker_harsh_winter
		}
		if = {
			limit = {
				has_province_modifier = winter_soldier_normal_winter
			}
			remove_province_modifier = winter_soldier_normal_winter
		}
		if = {
			limit = {
				has_province_modifier = winter_soldier_harsh_winter
			}
			remove_province_modifier = winter_soldier_harsh_winter
		}
		add_province_modifier = {
			name = winter_soldier_mild_winter
			duration = -1
		}
	}	
}

#winter_soldier.4 Counteract normal winter effects
province_event = {
	id = winter_soldier.4
	hide_window = yes
	
	trigger = {
		is_winter_trigger = yes
		OR = {
			region = world_beyond_the_wall
			region = world_the_wall
			region = world_north
		}
		owner = {
			NOR = {
				trait = white_walker
				culture = winter_wasteland
			}
		}
		has_province_modifier = asoiaf_normal_winter
	}
	
	mean_time_to_happen = {
		days = 7
	}
	
	option = {
		name = "Ok"
		if = {
			limit = {
				has_province_modifier = white_walker_mild_winter
			}
			remove_province_modifier = white_walker_mild_winter
		}
		if = {
			limit = {
				has_province_modifier = white_walker_normal_winter
			}
			remove_province_modifier = white_walker_normal_winter
		}
		if = {
			limit = {
				has_province_modifier = white_walker_harsh_winter
			}
			remove_province_modifier = white_walker_harsh_winter
		}
		if = {
			limit = {
				has_province_modifier = winter_soldier_mild_winter
			}
			remove_province_modifier = winter_soldier_mild_winter
		}
		if = {
			limit = {
				has_province_modifier = winter_soldier_harsh_winter
			}
			remove_province_modifier = winter_soldier_harsh_winter
		}
		add_province_modifier = {
			name = winter_soldier_normal_winter
			duration = -1
		}
	}
}

#winter_soldier.5 Counteract harsh winter effects
province_event = {
	id = winter_soldier.5
	hide_window = yes	
	
	trigger = {
		is_winter_trigger = yes
		OR = {
			region = world_beyond_the_wall
			region = world_the_wall
			region = world_north
		}
		owner = {
			NOR = {
				trait = white_walker
				culture = winter_wasteland
			}
		}
		has_province_modifier = asoiaf_harsh_winter
	}
	
	mean_time_to_happen = {
		days = 7
	}
	
	option = {
		name = "Ok"
		if = {
			limit = {
				has_province_modifier = white_walker_mild_winter
			}
			remove_province_modifier = white_walker_mild_winter
		}
		if = {
			limit = {
				has_province_modifier = white_walker_normal_winter
			}
			remove_province_modifier = white_walker_normal_winter
		}
		if = {
			limit = {
				has_province_modifier = white_walker_harsh_winter
			}
			remove_province_modifier = white_walker_harsh_winter
		}
		if = {
			limit = {
				has_province_modifier = winter_soldier_normal_winter
			}
			remove_province_modifier = winter_soldier_normal_winter
		}
		if = {
			limit = {
				has_province_modifier = winter_soldier_mild_winter
			}
			remove_province_modifier = winter_soldier_mild_winter
		}
		add_province_modifier = {
			name = winter_soldier_harsh_winter
			duration = -1
		}
	}
}

#winter_soldier.6 Counteract mild winter effects for White Walkers
province_event = {
	id = winter_soldier.6
	hide_window = yes
	
	trigger = {
		is_winter_trigger = yes
		owner = {
			OR = {
				trait = white_walker
				culture = winter_wasteland
			}
		}
		has_province_modifier = asoiaf_mild_winter
	}
	
	mean_time_to_happen = {
		days = 7
	}
	
	option = {
		name = "Ok"
		if = {
			limit = {
				has_province_modifier = winter_soldier_mild_winter
			}
			remove_province_modifier = winter_soldier_mild_winter
		}
		if = {
			limit = {
				has_province_modifier = winter_soldier_normal_winter
			}
			remove_province_modifier = winter_soldier_normal_winter
		}
		if = {
			limit = {
				has_province_modifier = winter_soldier_harsh_winter
			}
			remove_province_modifier = winter_soldier_harsh_winter
		}
		if = {
			limit = {
				has_province_modifier = white_walker_normal_winter
			}
			remove_province_modifier = white_walker_normal_winter
		}
		if = {
			limit = {
				has_province_modifier = white_walker_harsh_winter
			}
			remove_province_modifier = white_walker_harsh_winter
		}
		add_province_modifier = {
			name = white_walker_mild_winter
			duration = -1
		}
	}	
}

#winter_soldier.7 Counteract normal winter effects for White Walkers
province_event = {
	id = winter_soldier.7
	hide_window = yes
	
	trigger = {
		is_winter_trigger = yes
		owner= {
			OR = {
				trait = white_walker
				culture = winter_wasteland
			}
		}
		has_province_modifier = asoiaf_normal_winter
	}
	
	mean_time_to_happen = {
		days = 7
	}
	
	option = {
		name = "Ok"
		if = {
			limit = {
				has_province_modifier = winter_soldier_mild_winter
			}
			remove_province_modifier = winter_soldier_mild_winter
		}
		if = {
			limit = {
				has_province_modifier = winter_soldier_normal_winter
			}
			remove_province_modifier = winter_soldier_normal_winter
		}
		if = {
			limit = {
				has_province_modifier = winter_soldier_harsh_winter
			}
			remove_province_modifier = winter_soldier_harsh_winter
		}
		if = {
			limit = {
				has_province_modifier = white_walker_mild_winter
			}
			remove_province_modifier = white_walker_mild_winter
		}
		if = {
			limit = {
				has_province_modifier = white_walker_harsh_winter
			}
			remove_province_modifier = white_walker_harsh_winter
		}
		add_province_modifier = {
			name = white_walker_normal_winter
			duration = -1
		}
	}
}

#winter_soldier.8 Counteract harsh winter effects for White Walkers
province_event = {
	id = winter_soldier.8
	hide_window = yes	
	
	trigger = {
		is_winter_trigger = yes
		owner = {
			OR = {
				trait = white_walker
				culture = winter_wasteland
			}
		}
		has_province_modifier = asoiaf_harsh_winter
	}
	
	mean_time_to_happen = {
		days = 7
	}
	
	option = {
		name = "Ok"
		if = {
			limit = {
				has_province_modifier = winter_soldier_mild_winter
			}
			remove_province_modifier = winter_soldier_mild_winter
		}
		if = {
			limit = {
				has_province_modifier = winter_soldier_normal_winter
			}
			remove_province_modifier = winter_soldier_normal_winter
		}
		if = {
			limit = {
				has_province_modifier = winter_soldier_harsh_winter
			}
			remove_province_modifier = winter_soldier_harsh_winter
		}
		if = {
			limit = {
				has_province_modifier = white_walker_normal_winter
			}
			remove_province_modifier = white_walker_normal_winter
		}
		if = {
			limit = {
				has_province_modifier = white_walker_mild_winter
			}
			remove_province_modifier = white_walker_mild_winter
		}
		add_province_modifier = {
			name = white_walker_harsh_winter
			duration = -1
		}
	}
}

#winter_soldier.9 Remove modifiers when summer starts
province_event = {
	id = winter_soldier.9
	hide_window = yes
	
	trigger = {
		is_summer_trigger = yes
		OR = {
			has_province_modifier = winter_soldier_mild_winter
			has_province_modifier = winter_soldier_normal_winter
			has_province_modifier = winter_soldier_harsh_winter
			has_province_modifier = white_walker_mild_winter
			has_province_modifier = white_walker_normal_winter
			has_province_modifier = white_walker_harsh_winter
		}
		NOT = {
			has_global_flag = cold_winds
		}
	}
	
	mean_time_to_happen = {
		months = 3
	}
	
	option = {
		name = "OK"
		if = {
			limit = {
				has_province_modifier = winter_soldier_mild_winter
			}
			remove_province_modifier = winter_soldier_mild_winter
		}
		if = {
			limit = {
				has_province_modifier = winter_soldier_normal_winter
			}
			remove_province_modifier = winter_soldier_normal_winter
		}
		if = {
			limit = {
				has_province_modifier = winter_soldier_harsh_winter
			}
			remove_province_modifier = winter_soldier_harsh_winter
		}
		if = {
			limit = {
				has_province_modifier = white_walker_mild_winter
			}
			remove_province_modifier = white_walker_mild_winter
		}
		if = {
			limit = {
				has_province_modifier = white_walker_normal_winter
			}
			remove_province_modifier = white_walker_normal_winter
		}
		if = {
			limit = {
				has_province_modifier = white_walker_harsh_winter
			}
			remove_province_modifier = white_walker_harsh_winter
		}
	}
}