namespace = mbs_old_gods_blessing_rejected		

#mbs_old_gods_blessing_rejected.1 - Mormont Snowbear
narrative_event = {
	id = mbs_old_gods_blessing_rejected.1
	title = "EVTtitlembs_old_gods_blessing_rejected.1"
	desc = "EVTDESCmbs_old_gods_blessing_rejected.1"
	picture = "GFX_snowbear"
	
	is_triggered_only = yes

	immediate = {
		random_list = {
			66 = { 
				ROOT = {
					opinion = {
						modifier = opinion_disgruntled
						who = FROM
						years = 8
					}
				}
			}
			33 = {

			}
		}
	}
	
	option = {
		name = "EVTOPTAmbs_old_gods_blessing_rejected.1" #don't get snowbear
		
		ai_chance = {
			factor = 2
		}		
	}
}
#mbs_old_gods_blessing_rejected.2 - Mormont Snowbear
narrative_event = {
	id = mbs_old_gods_blessing_rejected.2
	title = "EVTtitlembs_old_gods_blessing_rejected.2"
	desc = "EVTDESCmbs_old_gods_blessing_rejected.2"
	picture = "GFX_brownbear"
	
	is_triggered_only = yes

	immediate = {
		random_list = {
			66 = { 
				ROOT = {
					opinion = {
						modifier = opinion_disgruntled
						who = FROM
						years = 8
					}
				}
			}
			33 = {

			}
		}
	}
	
	option = {
		name = "EVTOPTAmbs_old_gods_blessing_rejected.2" #don't get brownbear
		
		ai_chance = {
			factor = 2
		}		
	}
}
#mbs_old_gods_blessing_rejected.3 - Blackwood Raven
narrative_event = {
	id = mbs_old_gods_blessing_rejected.3
	title = "EVTtitlembs_old_gods_blessing_rejected.3"
	desc = "EVTDESCmbs_old_gods_blessing_rejected.3"
	picture = "GFX_crow"
	
	is_triggered_only = yes

	immediate = {
		random_list = {
			66 = { 
				ROOT = {
					opinion = {
						modifier = opinion_disgruntled
						who = FROM
						years = 8
					}
				}
			}
			33 = {

			}
		}
	}
	
	option = {
		name = "EVTOPTAmbs_old_gods_blessing_rejected.3" #don't get raven
		
		ai_chance = {
			factor = 2
		}		
	}
}
#mbs_old_gods_blessing_rejected.4 - Stark Direwolf
narrative_event = {
	id = mbs_old_gods_blessing_rejected.4
	title = "EVTtitlembs_old_gods_blessing_rejected.4"
	desc = "EVTDESCmbs_old_gods_blessing_rejected.4"
	picture = "GFX_wolf"
	
	is_triggered_only = yes

	immediate = {
		random_list = {
			66 = { 
				ROOT = {
					opinion = {
						modifier = opinion_disgruntled
						who = FROM
						years = 8
					}
				}
			}
			33 = {

			}
		}
	}
	
	option = {
		name = "EVTOPTAmbs_old_gods_blessing_rejected.4" #don't get wolf
		
		ai_chance = {
			factor = 2
		}		
	}
}
#mbs_old_gods_blessing_rejected.5 - Reed Lizardlion
narrative_event = {
	id = mbs_old_gods_blessing_rejected.5
	title = "EVTtitlembs_old_gods_blessing_rejected.5"
	desc = "EVTDESCmbs_old_gods_blessing_rejected.5"
	picture = "GFX_evt_hunting_scene"
	
	is_triggered_only = yes

	immediate = {
		random_list = {
			66 = { 
				ROOT = {
					opinion = {
						modifier = opinion_disgruntled
						who = FROM
						years = 8
					}
				}
			}
			33 = {

			}
		}
	}
	
	option = {
		name = "EVTOPTAmbs_old_gods_blessing_rejected.5" #don't get wolf
		
		ai_chance = {
			factor = 2
		}		
	}
}
#mbs_old_gods_blessing_rejected.5 - Banefort lion
narrative_event = {
	id = mbs_old_gods_blessing_rejected.6
	title = "EVTtitlembs_old_gods_blessing_rejected.6"
	desc = "EVTDESCmbs_old_gods_blessing_rejected.6"
	picture = "GFX_evt_hunting_scene"
	
	is_triggered_only = yes

	immediate = {
		random_list = {
			66 = { 
				ROOT = {
					opinion = {
						modifier = opinion_disgruntled
						who = FROM
						years = 8
					}
				}
			}
			33 = {

			}
		}
	}
	
	option = {
		name = "EVTOPTAmbs_old_gods_blessing_rejected.6" #don't get wolf
		
		ai_chance = {
			factor = 2
		}		
	}
}