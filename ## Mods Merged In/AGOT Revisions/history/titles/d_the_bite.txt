900.1.1={ 
	law=succ_primogeniture 
	law=cognatic_succession
	liege="e_vale"
}
###House Strickland
7969.1.1 = { holder=30001428 } # Robert (nc)
7998.6.6={
	liege="k_vale"
}
8006.1.1 = { holder=30011428 } # Anslem (nc)
8020.1.1 = { holder=30041428 } # Elwood (nc)
8049.1.1 = { holder=30081428 } # Amelryc (nc)
8073.1.1 = { holder=30091428 } # Edwyn (nc)
8093.1.1 = { holder=30101428 } # Titus (nc)
8123.1.1 = { holder=30151428 } # Hectyr (nc)
8176.1.1 = { holder=30231428 } # Pelleas (nc)
8192.1.1 = { holder=30241428 } # Quentyn (nc)
8195.3.7 = {
	liege = 0
}
###House Egen
8196.1.1 = { 
	holder=318175
	liege = k_vale
} # Albar (nc)
8248.1.1={
	holder = 94092 #Benedar
}
8279.1.1={
	holder = 94079 	#Loras Loras Egen
}