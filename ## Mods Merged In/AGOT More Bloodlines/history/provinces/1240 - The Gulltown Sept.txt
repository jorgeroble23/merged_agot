

# County Title
title = c_mbs_gulltown_sept

# Settlements
max_settlements = 1
b_mbs_gulltown_sept = temple

# Misc
culture = old_first_man
religion = old_gods

# History
1.1.1 = {
	
	b_mbs_gulltown_sept = tp_monastery_1

}
6700.1.1 = {
	culture = valeman
	religion = the_seven
}