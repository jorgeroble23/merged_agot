namespace = Ac
# What to do with defeated ruler...?
character_event = {
	id = Ac.7779
	desc = "EVTDESCconvert"
	picture = "GFX_tapestry_animals"
	border = GFX_event_normal_frame_war

	min_age = 16
	capable_only = yes

	is_triggered_only = yes


	option = {
		name = EVTOPTAconvert
		ai_chance = {
			factor = 50
		}
		custom_tooltip = { text = conv1 }
		event_target:target_loser = { imprison = ROOT }
		clear_event_target = target_loser
		clear_event_target = target_depose
	}
	option = {
		name = EVTOPTBconvert
		ai_chance = {
			factor = 25
		}
		custom_tooltip = { text = conv2 }
		event_target:target_loser = { 
			transfer_scaled_wealth = {
				from = PREV
				to = ROOT
				value = 2.5
			}
		}
		clear_event_target = target_loser
		clear_event_target = target_depose
	}
	option = {
		name = EVTOPTCconvert
		ai_chance = {
			factor = 0
		}
		trigger = {
			ROOT = { higher_tier_than = COUNT }
			event_target:target_loser = { lower_tier_than = DUKE }
		}
		custom_tooltip = { text = conv3 }
		ROOT = { prestige = 100 }
		event_target:target_loser = { 
			set_defacto_liege = ROOT 
			prestige = -100
		}
		clear_event_target = target_loser
		clear_event_target = target_depose
	}
	option = {
		name = EVTOPTDconvert
		ai_chance = {
			factor = 25
		}
		custom_tooltip = { text = conv4 }
		prestige = 50
		ROOT = { 
			make_tributary = { who = event_target:target_loser percentage = 0.4 }
			}
		event_target:target_loser = {
			opinion = {
				modifier = opinion_forced_tributary 
				who = ROOT
				years = 15
			}
		}
		clear_event_target = target_loser
		clear_event_target = target_depose
	}
	option = {
		name = EVTOPTEconvert
		ai_chance = {
			factor = 0
		}
		trigger = {
			ROOT = { piety = 800 }
			ROOT = { any_courtier = { has_minor_title = title_commander } }
			ROOT = { tier = KING }
			event_target:target_loser = { tier = DUKE }
		}
		custom_tooltip = { text = conv5 }
		ROOT = { piety = -800 }
			event_target:target_loser = { 
			abdicate_to = event_target:target_depose 
			prestige = -500
		}
		clear_event_target = target_loser
		clear_event_target = target_depose
	}
}