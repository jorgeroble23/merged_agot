bloodbloom = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
cataclysm = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
desolation = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
dread = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
exile = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
extinction = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
futility = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
headsmans_herald = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
houndsbreath = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
judgment = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
lionheart = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
prophecy = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	customizer = no	
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger
}
raging_storm = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
ravenwing = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
redeemer = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
remorse = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
ruinsword = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
shadows_fall = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
sovereign = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
spiderbite = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
valor = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
destiny = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
eternity = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
fate = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
oblivion = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	customizer = no	
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger
}