on_startup = {
	events = {
		TSP_1K_island_fix.3 # Removes Bay of Tusks Holy Site that crashes religion interface
	}
}

on_yearly_pulse = {
	events = {
#		TSP_1K_island_fix.2 # Back up in case the Thousand Islands Temple doesn't get the Holy Site
		TSP_1K_island_fix.4 # Forces the Pontifex of the Deep to be a Theocracy
		TSP_1KI.2 # Fish Gods clergyman becomes Unworthy
		TSP_1KI_story_mode.1 # Removes an immortal Citadel Grandmaster (AKA Dooley Swiggity if he reaches that high)
#		TSP_1K_island_fix.13 # Allows all future Old Ones worshippers to intermarry with the Fish Gods
		TSP_1KI_story_mode.10 # PUT THAT THING BACK WHERE IT CAME FROM, OR _SO_HELP_ME_
		TSP_1K_island_fix.3 # Removes Bay of Tusks Holy Site that crashes religion interface
	}
}

on_yearly_focus_pulse = {
	events = { }
	random_events = {
		1000 = 0
		350 = TSP_1KI.9 # Chance to feel sympathy for the Fish Gods if you have Theology Focus
	}
}

on_rel_head_chosen = {
	events = {
		TSP_1KI.3 # Punish a wicked Pontifex of the Deep
	}
}

#on_reform_religion = {
#	events = {
#		
#	}
#}

#on_birth = {
#	events = {
#		TSP_1K_island_fix.11 # Illegitimate Thousand Islander gets the name "Fish"
#	}
#}