# 232 - Wendwater

# County Title
title = c_chyttering_brook

# Settlements
max_settlements = 3
b_chyttering_brook = castle
b_wendwater = castle
#b_wendbridge = city

#b_orley = castle
#b_lowercliff = city


# Misc
culture = old_first_man
religion = old_gods

# History

1066.1.1 = {
	b_chyttering_brook = ca_asoiaf_crown_basevalue_1
	b_chyttering_brook = ca_asoiaf_crown_basevalue_2
	b_chyttering_brook = ca_asoiaf_crown_basevalue_3

	b_wendwater = ca_asoiaf_crown_basevalue_1
	b_wendwater = ca_asoiaf_crown_basevalue_2

	#b_wendbridge = ct_asoiaf_crown_basevalue_1
	#b_wendbridge = ct_asoiaf_crown_basevalue_2
}
6700.1.1 = { 
	culture = stormlander
	religion = the_seven
}