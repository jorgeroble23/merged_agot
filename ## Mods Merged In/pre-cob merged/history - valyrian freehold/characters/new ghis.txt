199360025 = { #Grazdan the Great
	name="Grazdan"	
	
	martial = 6
	diplomacy = 8
	intrigue = 7
	stewardship = 9
	learning = 8

	religion="harpy"
	culture="ghiscari"
	
	add_trait = grey_eminence
	add_trait = ambitious
	add_trait = diligent
	add_trait = authoritative
	add_trait = quick
	
	give_nickname = nick_the_great
	
	disallow_random_traits = yes

	2000.1.1 = {birth="2000.1.1"}
	2016.1.1 = {
		add_trait = skilled_warrior
	}
	2068.1.1 = {death="3068.1.1"}
}