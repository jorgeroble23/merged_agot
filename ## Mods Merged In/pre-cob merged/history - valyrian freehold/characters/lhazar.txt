###Izven of Lhazosh###
10055916 = {
	name="Jommo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	7565.1.1 = {birth="7565.1.1"}
	7581.1.1 = {add_trait=poor_warrior}
	7583.1.1 = {add_spouse=40055916}
	7618.1.1 = {death="7618.1.1"}
}
40055916 = {
	name="Jhiqui"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7565.1.1 = {birth="7565.1.1"}
	7618.1.1 = {death="7618.1.1"}
}
10155916 = {
	name="Jhigi"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10055916
	mother=40055916

	7584.1.1 = {birth="7584.1.1"}
	7648.1.1 = {death="7648.1.1"}
}
10255916 = {
	name="Zikki"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10055916
	mother=40055916

	7586.1.1 = {birth="7586.1.1"}
	7629.1.1 = {death="7629.1.1"}
}
10355916 = {
	name="Fogo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10055916
	mother=40055916

	7587.1.1 = {birth="7587.1.1"}
	7603.1.1 = {add_trait=poor_warrior}
	7605.1.1 = {add_spouse=40355916}
	7635.1.1 = {death="7635.1.1"}
}
40355916 = {
	name="Zikki"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7587.1.1 = {birth="7587.1.1"}
	7635.1.1 = {death="7635.1.1"}
}
10455916 = {
	name="Rommo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10055916
	mother=40055916

	7588.1.1 = {birth="7588.1.1"}
	7604.1.1 = {add_trait=poor_warrior}
	7628.1.1 = {death="7628.1.1"}
}
10555916 = {
	name="Rommo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10055916
	mother=40055916

	7590.1.1 = {birth="7590.1.1"}
	7606.1.1 = {add_trait=poor_warrior}
	7666.1.1 = {death="7666.1.1"}
}
10655916 = {
	name="Zollo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10355916
	mother=40355916

	7616.1.1 = {birth="7616.1.1"}
	7632.1.1 = {add_trait=trained_warrior}
	7634.1.1 = {add_spouse=40655916}
	7687.1.1 = {death="7687.1.1"}
}
40655916 = {
	name="Zikki"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7616.1.1 = {birth="7616.1.1"}
	7687.1.1 = {death="7687.1.1"}
}
10755916 = {
	name="Mirri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10355916
	mother=40355916

	7617.1.1 = {birth="7617.1.1"}
	7654.1.1 = {death="7654.1.1"}
}
10855916 = {
	name="Aggo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10655916
	mother=40655916

	7637.1.1 = {birth="7637.1.1"}
	7653.1.1 = {add_trait=poor_warrior}
	7655.1.1 = {add_spouse=40855916}
	7694.1.1 = {death="7694.1.1"}
}
40855916 = {
	name="Jhigi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7637.1.1 = {birth="7637.1.1"}
	7694.1.1 = {death="7694.1.1"}
}
10955916 = {
	name="Zolli"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10655916
	mother=40655916

	7640.1.1 = {birth="7640.1.1"}
	7709.1.1 = {death="7709.1.1"}
}
11055916 = {
	name="Rhogoro"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10655916
	mother=40655916

	7641.1.1 = {birth="7641.1.1"}
	7657.1.1 = {add_trait=trained_warrior}
	7698.1.1 = {death="7698.1.1"}
}
11155916 = {
	name="Iggo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10855916
	mother=40855916

	7662.1.1 = {birth="7662.1.1"}
	7678.1.1 = {add_trait=trained_warrior}
	7680.1.1 = {add_spouse=41155916}
	7738.1.1 = {death="7738.1.1"}
}
41155916 = {
	name="Rimmi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7662.1.1 = {birth="7662.1.1"}
	7738.1.1 = {death="7738.1.1"}
}
11255916 = {
	name="Arakh"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10855916
	mother=40855916

	7664.1.1 = {birth="7664.1.1"}
	7680.1.1 = {add_trait=poor_warrior}
	7682.1.1 = {add_spouse=41255916}
	7725.1.1 = {death="7725.1.1"}
}
41255916 = {
	name="Coholli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7664.1.1 = {birth="7664.1.1"}
	7725.1.1 = {death="7725.1.1"}
}
11355916 = {
	name="Quiri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11155916
	mother=41155916

	7684.1.1 = {birth="7684.1.1"}
	7763.1.1 = {death="7763.1.1"}
}
11455916 = {
	name="Mago"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11255916
	mother=41255916

	7689.1.1 = {birth="7689.1.1"}
	7705.1.1 = {add_trait=trained_warrior}
	7707.1.1 = {add_spouse=41455916}
	7731.1.1 = {death="7731.1.1"}
}
41455916 = {
	name="Coholli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7689.1.1 = {birth="7689.1.1"}
	7731.1.1 = {death="7731.1.1"}
}
11555916 = {
	name="Fogo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11255916
	mother=41255916

	7691.1.1 = {birth="7691.1.1"}
	7707.1.1 = {add_trait=poor_warrior}
	7744.1.1 = {death="7744.1.1"}
}
11655916 = {
	name="Zolli"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11255916
	mother=41255916

	7692.1.1 = {birth="7692.1.1"}
	7757.1.1 = {death="7757.1.1"}
}
11755916 = {
	name="Ogo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11455916
	mother=41455916

	7708.1.1 = {birth="7708.1.1"}
	7724.1.1 = {add_trait=poor_warrior}
	7726.1.1 = {add_spouse=41755916}
	7776.1.1 = {death="7776.1.1"}
}
41755916 = {
	name="Mirri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7708.1.1 = {birth="7708.1.1"}
	7776.1.1 = {death="7776.1.1"}
}
11855916 = {
	name="Jhigi"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11455916
	mother=41455916

	7709.1.1 = {birth="7709.1.1"}
	7767.1.1 = {death="7767.1.1"}
}
11955916 = {
	name="Mago"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11455916
	mother=41455916

	7711.1.1 = {birth="7711.1.1"}
	7727.1.1 = {add_trait=poor_warrior}
	7758.1.1 = {death="7758.1.1"}
}
12055916 = {
	name="Qothi"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11455916
	mother=41455916

	7713.1.1 = {birth="7713.1.1"}
	7742.1.1 = {death = {death_reason = death_murder}}
}
12155916 = {
	name="Zollo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11755916
	mother=41755916

	7727.1.1 = {birth="7727.1.1"}
	7743.1.1 = {add_trait=trained_warrior}
	7745.1.1 = {add_spouse=42155916}
	7785.1.1 = {death="7785.1.1"}
}
42155916 = {
	name="Zikki"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7727.1.1 = {birth="7727.1.1"}
	7785.1.1 = {death="7785.1.1"}
}
12255916 = {
	name="Rakharo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11755916
	mother=41755916

	7729.1.1 = {birth="7729.1.1"}
	7745.1.1 = {add_trait=trained_warrior}
	7760.1.1 = {death="7760.1.1"}
}
12355916 = {
	name="Mirri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11755916
	mother=41755916

	7730.1.1 = {birth="7730.1.1"}
	7778.1.1 = {death="7778.1.1"}
}
12455916 = {
	name="Mago"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11755916
	mother=41755916

	7732.1.1 = {birth="7732.1.1"}
	7748.1.1 = {add_trait=poor_warrior}
	7804.1.1 = {death="7804.1.1"}
}
12555916 = {
	name="Rimmi"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11755916
	mother=41755916

	7733.1.1 = {birth="7733.1.1"}
	7777.1.1 = {death="7777.1.1"}
}
12655916 = {
	name="Bharbo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12155916
	mother=42155916

	7753.1.1 = {birth="7753.1.1"}
	7769.1.1 = {add_trait=poor_warrior}
	7771.1.1 = {add_spouse=42655916}
	7798.1.1 = {death="7798.1.1"}
}
42655916 = {
	name="Zikki"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7753.1.1 = {birth="7753.1.1"}
	7798.1.1 = {death="7798.1.1"}
}
12755916 = {
	name="Rimmi"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12155916
	mother=42155916

	7754.1.1 = {birth="7754.1.1"}
	7803.1.1 = {death="7803.1.1"}
}
12855916 = {
	name="Zolli"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12155916
	mother=42155916

	7755.1.1 = {birth="7755.1.1"}
	7803.1.1 = {death = {death_reason = death_accident}}
}
12955916 = {
	name="Haggo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12655916
	mother=42655916

	7774.1.1 = {birth="7774.1.1"}
	7790.1.1 = {add_trait=trained_warrior}
	7792.1.1 = {add_spouse=42955916}
	7839.1.1 = {death="7839.1.1"}
}
42955916 = {
	name="Jhigi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7774.1.1 = {birth="7774.1.1"}
	7839.1.1 = {death="7839.1.1"}
}
13055916 = {
	name="Rakharo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12655916
	mother=42655916

	7775.1.1 = {birth="7775.1.1"}
	7791.1.1 = {add_trait=trained_warrior}
	7793.1.1 = {add_spouse=43055916}
	7825.1.1 = {death="7825.1.1"}
}
43055916 = {
	name="Irri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7775.1.1 = {birth="7775.1.1"}
	7825.1.1 = {death="7825.1.1"}
}
13155916 = {
	name="Mothi"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12655916
	mother=42655916

	7777.1.1 = {birth="7777.1.1"}
	7827.1.1 = {death="7827.1.1"}
}
13255916 = {
	name="Mirri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12955916
	mother=42955916

	7801.1.1 = {birth="7801.1.1"}
	7845.1.1 = {death="7845.1.1"}
}
13355916 = {
	name="Rakhiri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13055916
	mother=43055916

	7800.1.1 = {birth="7800.1.1"}
	7876.1.1 = {death="7876.1.1"}
}
13455916 = {
	name="Cohollo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13055916
	mother=43055916

	7801.1.1 = {birth="7801.1.1"}
	7817.1.1 = {add_trait=poor_warrior}
	7819.1.1 = {add_spouse=43455916}
	7866.1.1 = {death="7866.1.1"}
}
43455916 = {
	name="Coholli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7801.1.1 = {birth="7801.1.1"}
	7866.1.1 = {death="7866.1.1"}
}
13555916 = {
	name="Rakharo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13055916
	mother=43055916

	7802.1.1 = {birth="7802.1.1"}
	7818.1.1 = {add_trait=poor_warrior}
	7855.1.1 = {death="7855.1.1"}
}
13655916 = {
	name="Pono"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13455916
	mother=43455916

	7834.1.1 = {birth="7834.1.1"}
	7850.1.1 = {add_trait=trained_warrior}
	7852.1.1 = {add_spouse=43655916}
	7887.1.1 = {death="7887.1.1"}
}
43655916 = {
	name="Jhiqui"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7834.1.1 = {birth="7834.1.1"}
	7887.1.1 = {death="7887.1.1"}
}
13755916 = {
	name="Rhogoro"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13455916
	mother=43455916

	7837.1.1 = {birth="7837.1.1"}
	7853.1.1 = {add_trait=trained_warrior}
	7855.1.1 = {add_spouse=43755916}
	7869.1.1 = {death = {death_reason = death_murder}}
}
43755916 = {
	name="Kovarri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7837.1.1 = {birth="7837.1.1"}
	7869.1.1 = {death="7869.1.1"}
}
13855916 = {
	name="Qotho"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13455916
	mother=43455916

	7840.1.1 = {birth="7840.1.1"}
	7856.1.1 = {add_trait=poor_warrior}
	7890.1.1 = {death="7890.1.1"}
}
13955916 = {
	name="Mirri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13655916
	mother=43655916

	7858.1.1 = {birth="7858.1.1"}
}
14055916 = {
	name="Rakharo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13755916
	mother=43755916

	7857.1.1 = {birth="7857.1.1"}
	7873.1.1 = {add_trait=poor_warrior}
	7886.1.1 = {add_spouse=44055916}
}
44055916 = {
	name="Jhigi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7868.1.1 = {birth="7868.1.1"}
}
14155916 = {
	name="Coholli"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13755916
	mother=43755916

	7860.1.1 = {birth="7860.1.1"}
}
14255916 = {
	name="Mirri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=14055916
	mother=44055916

	7894.1.1 = {birth="7894.1.1"}
}
14355916 = {
	name="Motho"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=14055916
	mother=44055916

	7897.1.1 = {birth="7897.1.1"}
	7913.1.1 = {add_trait=poor_warrior}
}
14455916 = {
	name="Quiri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=14055916
	mother=44055916

	7898.1.1 = {birth="7898.1.1"}
}
14555916 = {
	name="Ogo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=14055916
	mother=44055916

	7899.1.1 = {birth="7899.1.1"}
	7915.1.1 = {add_trait=poor_warrior}
}
###Sheqethi of Hesh###
10055923 = {
	name="Pono"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	7553.1.1 = {birth="7553.1.1"}
	7569.1.1 = {add_trait=poor_warrior}
	7571.1.1 = {add_spouse=40055923}
	7610.1.1 = {death="7610.1.1"}
}
40055923 = {
	name="Rimmi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7553.1.1 = {birth="7553.1.1"}
	7610.1.1 = {death="7610.1.1"}
}
10155923 = {
	name="Rhogoro"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10055923
	mother=40055923

	7582.1.1 = {birth="7582.1.1"}
	7598.1.1 = {add_trait=trained_warrior}
	7653.1.1 = {death="7653.1.1"}
}
10255923 = {
	name="Rhogiri"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10055923
	mother=40055923

	7584.1.1 = {birth="7584.1.1"}
	7638.1.1 = {death="7638.1.1"}
}
10355923 = {
	name="Motho"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10055923
	mother=40055923

	7585.1.1 = {birth="7585.1.1"}
	7601.1.1 = {add_trait=trained_warrior}
	7603.1.1 = {add_spouse=40355923}
	7657.1.1 = {death="7657.1.1"}
}
40355923 = {
	name="Mothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7585.1.1 = {birth="7585.1.1"}
	7657.1.1 = {death="7657.1.1"}
}
10455923 = {
	name="Rakharo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10355923
	mother=40355923

	7603.1.1 = {birth="7603.1.1"}
	7619.1.1 = {add_trait=poor_warrior}
	7680.1.1 = {death="7680.1.1"}
}
10555923 = {
	name="Jhogo"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10355923
	mother=40355923

	7604.1.1 = {birth="7604.1.1"}
	7620.1.1 = {add_trait=trained_warrior}
	7622.1.1 = {add_spouse=40555923}
	7664.1.1 = {death = {death_reason = death_accident}}
}
40555923 = {
	name="Aggi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7604.1.1 = {birth="7604.1.1"}
	7664.1.1 = {death="7664.1.1"}
}
10655923 = {
	name="Rhogiri"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10555923
	mother=40555923

	7623.1.1 = {birth="7623.1.1"}
	7666.1.1 = {death="7666.1.1"}
}
10755923 = {
	name="Pono"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10555923
	mother=40555923

	7626.1.1 = {birth="7626.1.1"}
	7642.1.1 = {add_trait=poor_warrior}
	7644.1.1 = {add_spouse=40755923}
	7682.1.1 = {death="7682.1.1"}
}
40755923 = {
	name="Rhogiri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7626.1.1 = {birth="7626.1.1"}
	7682.1.1 = {death="7682.1.1"}
}
10855923 = {
	name="Coholli"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10555923
	mother=40555923

	7627.1.1 = {birth="7627.1.1"}
	7673.1.1 = {death="7673.1.1"}
}
10955923 = {
	name="Malakho"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10555923
	mother=40555923

	7629.1.1 = {birth="7629.1.1"}
	7645.1.1 = {add_trait=poor_warrior}
	7647.1.1 = {add_spouse=40955923}
	7676.1.1 = {death="7676.1.1"}
}
40955923 = {
	name="Aggi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7629.1.1 = {birth="7629.1.1"}
	7676.1.1 = {death="7676.1.1"}
}
11055923 = {
	name="Jhigi"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10555923
	mother=40555923

	7632.1.1 = {birth="7632.1.1"}
	7670.1.1 = {death="7670.1.1"}
}
11155923 = {
	name="Zikki"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10955923
	mother=40955923

	7649.1.1 = {birth="7649.1.1"}
	7715.1.1 = {death="7715.1.1"}
}
11255923 = {
	name="Aggi"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10955923
	mother=40955923

	7651.1.1 = {birth="7651.1.1"}
	7696.1.1 = {death="7696.1.1"}
}
11355923 = {
	name="Zollo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10755923
	mother=40755923

	7647.1.1 = {birth="7647.1.1"}
	7663.1.1 = {add_trait=trained_warrior}
	7705.1.1 = {death="7705.1.1"}
}
11455923 = {
	name="Fogo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10755923
	mother=40755923

	7648.1.1 = {birth="7648.1.1"}
	7664.1.1 = {add_trait=trained_warrior}
	7715.1.1 = {death="7715.1.1"}
}
11555923 = {
	name="Jommo"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10755923
	mother=40755923

	7649.1.1 = {birth="7649.1.1"}
	7665.1.1 = {add_trait=poor_warrior}
	7667.1.1 = {add_spouse=41555923}
	7724.1.1 = {death = {death_reason = death_accident}}
}
41555923 = {
	name="Aggi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7649.1.1 = {birth="7649.1.1"}
	7724.1.1 = {death="7724.1.1"}
}
11655923 = {
	name="Kovarri"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10755923
	mother=40755923

	7652.1.1 = {birth="7652.1.1"}
	7701.1.1 = {death="7701.1.1"}
}
11755923 = {
	name="Rimmi"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11555923
	mother=41555923

	7674.1.1 = {birth="7674.1.1"}
	7743.1.1 = {death="7743.1.1"}
}
11855923 = {
	name="Rakharo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11555923
	mother=41555923

	7675.1.1 = {birth="7675.1.1"}
	7691.1.1 = {add_trait=poor_warrior}
	7693.1.1 = {add_spouse=41855923}
	7724.1.1 = {death="7724.1.1"}
}
41855923 = {
	name="Zolli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7675.1.1 = {birth="7675.1.1"}
	7724.1.1 = {death="7724.1.1"}
}
11955923 = {
	name="Pono"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11855923
	mother=41855923

	7696.1.1 = {birth="7696.1.1"}
	7706.1.1 = {death = {death_reason = death_accident}}
}
12055923 = {
	name="Motho"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11855923
	mother=41855923

	7698.1.1 = {birth="7698.1.1"}
	7714.1.1 = {add_trait=trained_warrior}
	7772.1.1 = {death="7772.1.1"}
}
12155923 = {
	name="Zikki"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11855923
	mother=41855923

	7699.1.1 = {birth="7699.1.1"}
	7744.1.1 = {death="7744.1.1"}
}
12255923 = {
	name="Jhigi"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11855923
	mother=41855923

	7701.1.1 = {birth="7701.1.1"}
	7710.1.1 = {death = {death_reason = death_accident}}
}
12355923 = {
	name="Drogo"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11855923
	mother=41855923

	7704.1.1 = {birth="7704.1.1"}
	7720.1.1 = {add_trait=trained_warrior}
	7722.1.1 = {add_spouse=42355923}
	7760.1.1 = {death="7760.1.1"}
}
42355923 = {
	name="Mothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7704.1.1 = {birth="7704.1.1"}
	7760.1.1 = {death="7760.1.1"}
}
12455923 = {
	name="Jhiqui"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12355923
	mother=42355923

	7737.1.1 = {birth="7737.1.1"}
	7790.1.1 = {death="7790.1.1"}
}
12555923 = {
	name="Moro"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12355923
	mother=42355923

	7740.1.1 = {birth="7740.1.1"}
	7756.1.1 = {add_trait=trained_warrior}
	7758.1.1 = {add_spouse=42555923}
	7773.1.1 = {death="7773.1.1"}
}
42555923 = {
	name="Zolli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7740.1.1 = {birth="7740.1.1"}
	7773.1.1 = {death="7773.1.1"}
}
12655923 = {
	name="Rommo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12555923
	mother=42555923

	7764.1.1 = {birth="7764.1.1"}
	7780.1.1 = {add_trait=trained_warrior}
	7798.1.1 = {death="7798.1.1"}
}
12755923 = {
	name="Rakharo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12555923
	mother=42555923

	7765.1.1 = {birth="7765.1.1"}
	7781.1.1 = {add_trait=trained_warrior}
	7808.1.1 = {death="7808.1.1"}
}
12855923 = {
	name="Zekko"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12555923
	mother=42555923

	7766.1.1 = {birth="7766.1.1"}
	7782.1.1 = {add_trait=poor_warrior}
	7784.1.1 = {add_spouse=42855923}
	7820.1.1 = {death="7820.1.1"}
}
42855923 = {
	name="Jhiqui"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7766.1.1 = {birth="7766.1.1"}
	7820.1.1 = {death="7820.1.1"}
}
12955923 = {
	name="Jommo"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12855923
	mother=42855923

	7791.1.1 = {birth="7791.1.1"}
	7807.1.1 = {add_trait=trained_warrior}
	7809.1.1 = {add_spouse=42955923}
	7832.1.1 = {death="7832.1.1"}
}
42955923 = {
	name="Qothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7791.1.1 = {birth="7791.1.1"}
	7832.1.1 = {death="7832.1.1"}
}
13055923 = {
	name="Rommo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12955923
	mother=42955923

	7815.1.1 = {birth="7815.1.1"}
	7831.1.1 = {add_trait=trained_warrior}
	7862.1.1 = {death = {death_reason = death_accident}}
}
13155923 = {
	name="Iggi"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12955923
	mother=42955923

	7816.1.1 = {birth="7816.1.1"}
	7869.1.1 = {death="7869.1.1"}
}
13255923 = {
	name="Qotho"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12955923
	mother=42955923

	7818.1.1 = {birth="7818.1.1"}
	7834.1.1 = {add_trait=trained_warrior}
	7836.1.1 = {add_spouse=43255923}
	7870.1.1 = {death="7870.1.1"}
}
43255923 = {
	name="Mothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7818.1.1 = {birth="7818.1.1"}
	7870.1.1 = {death="7870.1.1"}
}
13355923 = {
	name="Rakharo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=13255923
	mother=43255923

	7842.1.1 = {birth="7842.1.1"}
	7858.1.1 = {add_trait=poor_warrior}
}
13455923 = {
	name="Iggo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=13255923
	mother=43255923

	7845.1.1 = {birth="7845.1.1"}
	7861.1.1 = {add_trait=poor_warrior}
}
13555923 = {
	name="Mago"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=13255923
	mother=43255923

	7846.1.1 = {birth="7846.1.1"}
	7862.1.1 = {add_trait=trained_warrior}
	7864.1.1 = {add_spouse=43555923}
}
43555923 = {
	name="Mothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7846.1.1 = {birth="7846.1.1"}
}
13655923 = {
	name="Malakho"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=13555923
	mother=43555923

	7879.1.1 = {birth="7879.1.1"}
	7895.1.1 = {add_trait=poor_warrior}
}
13755923 = {
	name="Jhaquo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=13555923
	mother=43555923

	7881.1.1 = {birth="7881.1.1"}
	7897.1.1 = {add_trait=poor_warrior}
}
13855923 = {
	name="Rimmi"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=13555923
	mother=43555923

	7883.1.1 = {birth="7883.1.1"}
}
###Drogikh of Kosrak###
10055901 = {
	name="Moro"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	7532.1.1 = {birth="7532.1.1"}
	7548.1.1 = {add_trait=trained_warrior}
	7550.1.1 = {add_spouse=40055901}
	7604.1.1 = {death="7604.1.1"}
}
40055901 = {
	name="Irri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7532.1.1 = {birth="7532.1.1"}
	7604.1.1 = {death="7604.1.1"}
}
10155901 = {
	name="Drogo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10055901
	mother=40055901

	7561.1.1 = {birth="7561.1.1"}
	7577.1.1 = {add_trait=poor_warrior}
	7579.1.1 = {add_spouse=40155901}
	7625.1.1 = {death="7625.1.1"}
}
40155901 = {
	name="Mirri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7561.1.1 = {birth="7561.1.1"}
	7625.1.1 = {death="7625.1.1"}
}
10255901 = {
	name="Jhaquo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10055901
	mother=40055901

	7563.1.1 = {birth="7563.1.1"}
	7579.1.1 = {add_trait=poor_warrior}
	7607.1.1 = {death="7607.1.1"}
}
10355901 = {
	name="Rakhiri"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10055901
	mother=40055901

	7564.1.1 = {birth="7564.1.1"}
	7630.1.1 = {death="7630.1.1"}
}
10455901 = {
	name="Mago"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10055901
	mother=40055901

	7567.1.1 = {birth="7567.1.1"}
	7583.1.1 = {add_trait=poor_warrior}
	7585.1.1 = {add_spouse=40455901}
	7646.1.1 = {death="7646.1.1"}
}
40455901 = {
	name="Zolli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7567.1.1 = {birth="7567.1.1"}
	7646.1.1 = {death="7646.1.1"}
}
10555901 = {
	name="Pono"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10155901
	mother=40155901

	7589.1.1 = {birth="7589.1.1"}
	7605.1.1 = {add_trait=poor_warrior}
	7650.1.1 = {death="7650.1.1"}
}
10655901 = {
	name="Iggi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10155901
	mother=40155901

	7590.1.1 = {birth="7590.1.1"}
	7623.1.1 = {death="7623.1.1"}
}
10755901 = {
	name="Aggi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10455901
	mother=40455901

	7595.1.1 = {birth="7595.1.1"}
	7651.1.1 = {death="7651.1.1"}
}
10855901 = {
	name="Ogo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10455901
	mother=40455901

	7596.1.1 = {birth="7596.1.1"}
	7612.1.1 = {add_trait=poor_warrior}
	7614.1.1 = {add_spouse=40855901}
	7647.1.1 = {death="7647.1.1"}
}
40855901 = {
	name="Jhigi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7596.1.1 = {birth="7596.1.1"}
	7647.1.1 = {death="7647.1.1"}
}
10955901 = {
	name="Zikki"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10855901
	mother=40855901

	7628.1.1 = {birth="7628.1.1"}
	7686.1.1 = {death = {death_reason = death_murder}}
}
11055901 = {
	name="Rakharo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10855901
	mother=40855901

	7629.1.1 = {birth="7629.1.1"}
	7645.1.1 = {add_trait=poor_warrior}
	7687.1.1 = {death="7687.1.1"}
}
11155901 = {
	name="Cohollo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10855901
	mother=40855901

	7631.1.1 = {birth="7631.1.1"}
	7647.1.1 = {add_trait=trained_warrior}
	7649.1.1 = {add_spouse=41155901}
	7698.1.1 = {death="7698.1.1"}
}
41155901 = {
	name="Irri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7631.1.1 = {birth="7631.1.1"}
	7698.1.1 = {death="7698.1.1"}
}
11255901 = {
	name="Jhaquo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11155901
	mother=41155901

	7651.1.1 = {birth="7651.1.1"}
	7667.1.1 = {add_trait=poor_warrior}
	7669.1.1 = {add_spouse=41255901}
	7683.1.1 = {death = {death_reason = death_battle}}
}
41255901 = {
	name="Aggi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7651.1.1 = {birth="7651.1.1"}
	7683.1.1 = {death="7683.1.1"}
}
11355901 = {
	name="Haggo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11155901
	mother=41155901

	7654.1.1 = {birth="7654.1.1"}
	7670.1.1 = {add_trait=poor_warrior}
	7698.1.1 = {death="7698.1.1"}
}
11455901 = {
	name="Cohollo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11255901
	mother=41255901

	7674.1.1 = {birth="7674.1.1"}
	7690.1.1 = {add_trait=poor_warrior}
	7692.1.1 = {add_spouse=41455901}
	7728.1.1 = {death="7728.1.1"}
}
41455901 = {
	name="Kovarri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7674.1.1 = {birth="7674.1.1"}
	7728.1.1 = {death="7728.1.1"}
}
11555901 = {
	name="Zikki"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11255901
	mother=41255901

	7676.1.1 = {birth="7676.1.1"}
	7698.1.1 = {death="7698.1.1"}
}
11655901 = {
	name="Jommo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11255901
	mother=41255901

	7679.1.1 = {birth="7679.1.1"}
	7695.1.1 = {add_trait=poor_warrior}
	7697.1.1 = {add_spouse=41655901}
	7710.1.1 = {death="7710.1.1"}
}
41655901 = {
	name="Coholli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7679.1.1 = {birth="7679.1.1"}
	7710.1.1 = {death="7710.1.1"}
}
11755901 = {
	name="Zikki"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11255901
	mother=41255901

	7682.1.1 = {birth="7682.1.1"}
	7746.1.1 = {death="7746.1.1"}
}
11855901 = {
	name="Qothi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11655901
	mother=41655901

	7700.1.1 = {birth="7700.1.1"}
	7740.1.1 = {death="7740.1.1"}
}
11955901 = {
	name="Jhigi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11455901
	mother=41455901

	7696.1.1 = {birth="7696.1.1"}
	7753.1.1 = {death="7753.1.1"}
}
12055901 = {
	name="Kovarro"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11455901
	mother=41455901

	7698.1.1 = {birth="7698.1.1"}
	7714.1.1 = {add_trait=poor_warrior}
	7761.1.1 = {death="7761.1.1"}
}
12155901 = {
	name="Qothi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11455901
	mother=41455901

	7700.1.1 = {birth="7700.1.1"}
	7771.1.1 = {death="7771.1.1"}
}
12255901 = {
	name="Rakharo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11455901
	mother=41455901

	7703.1.1 = {birth="7703.1.1"}
	7719.1.1 = {add_trait=poor_warrior}
	7721.1.1 = {add_spouse=42255901}
	7744.1.1 = {death="7744.1.1"}
}
42255901 = {
	name="Mothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7703.1.1 = {birth="7703.1.1"}
	7744.1.1 = {death="7744.1.1"}
}
12355901 = {
	name="Jhiqui"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12255901
	mother=42255901

	7724.1.1 = {birth="7724.1.1"}
	7773.1.1 = {death="7773.1.1"}
}
12455901 = {
	name="Fogo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12255901
	mother=42255901

	7727.1.1 = {birth="7727.1.1"}
	7743.1.1 = {add_trait=trained_warrior}
	7801.1.1 = {death="7801.1.1"}
}
12555901 = {
	name="Jhaquo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12255901
	mother=42255901

	7729.1.1 = {birth="7729.1.1"}
	7745.1.1 = {add_trait=poor_warrior}
	7747.1.1 = {add_spouse=42555901}
	7783.1.1 = {death="7783.1.1"}
}
42555901 = {
	name="Mothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7729.1.1 = {birth="7729.1.1"}
	7783.1.1 = {death="7783.1.1"}
}
12655901 = {
	name="Rommo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12555901
	mother=42555901

	7755.1.1 = {birth="7755.1.1"}
	7771.1.1 = {add_trait=poor_warrior}
	7821.1.1 = {death="7821.1.1"}
}
12755901 = {
	name="Zollo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12555901
	mother=42555901

	7758.1.1 = {birth="7758.1.1"}
	7774.1.1 = {add_trait=poor_warrior}
	7776.1.1 = {add_spouse=42755901}
	7817.1.1 = {death="7817.1.1"}
}
42755901 = {
	name="Zolli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7758.1.1 = {birth="7758.1.1"}
	7817.1.1 = {death="7817.1.1"}
}
12855901 = {
	name="Iggi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12555901
	mother=42555901

	7759.1.1 = {birth="7759.1.1"}
	7808.1.1 = {death="7808.1.1"}
}
12955901 = {
	name="Motho"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12755901
	mother=42755901

	7791.1.1 = {birth="7791.1.1"}
	7807.1.1 = {add_trait=poor_warrior}
	7848.1.1 = {death = {death_reason = death_murder}}
}
13055901 = {
	name="Jhiqui"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12755901
	mother=42755901

	7794.1.1 = {birth="7794.1.1"}
	7847.1.1 = {death="7847.1.1"}
}
13155901 = {
	name="Pono"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12755901
	mother=42755901

	7795.1.1 = {birth="7795.1.1"}
	7811.1.1 = {add_trait=trained_warrior}
	7813.1.1 = {add_spouse=43155901}
	7850.1.1 = {death="7850.1.1"}
}
43155901 = {
	name="Zolli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7795.1.1 = {birth="7795.1.1"}
	7850.1.1 = {death="7850.1.1"}
}
13255901 = {
	name="Jhiqui"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12755901
	mother=42755901

	7796.1.1 = {birth="7796.1.1"}
	7836.1.1 = {death="7836.1.1"}
}
13355901 = {
	name="Qotho"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13155901
	mother=43155901

	7828.1.1 = {birth="7828.1.1"}
	7844.1.1 = {add_trait=trained_warrior}
	7882.1.1 = {death="7882.1.1"}
}
13455901 = {
	name="Rakharo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13155901
	mother=43155901

	7829.1.1 = {birth="7829.1.1"}
	7845.1.1 = {add_trait=trained_warrior}
	7899.1.1 = {death="7899.1.1"}
}
13555901 = {
	name="Jhaquo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13155901
	mother=43155901

	7831.1.1 = {birth="7831.1.1"}
	7847.1.1 = {add_trait=trained_warrior}
	7849.1.1 = {add_spouse=43555901}
	7879.1.1 = {death="7879.1.1"}
}
43555901 = {
	name="Jhiqui"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7831.1.1 = {birth="7831.1.1"}
	7879.1.1 = {death="7879.1.1"}
}
13655901 = {
	name="Rhogiri"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13155901
	mother=43155901

	7834.1.1 = {birth="7834.1.1"}
	7908.1.1 = {death="7908.1.1"}
}
13755901 = {
	name="Rakhiri"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13555901
	mother=43555901

	7859.1.1 = {birth="7859.1.1"}
}
13855901 = {
	name="Moro"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13555901
	mother=43555901

	7862.1.1 = {birth="7862.1.1"}
	7878.1.1 = {add_trait=poor_warrior}
	7880.1.1 = {add_spouse=43855901}
}
43855901 = {
	name="Zolli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7862.1.1 = {birth="7862.1.1"}
}
13955901 = {
	name="Drogo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13555901
	mother=43555901

	7864.1.1 = {birth="7864.1.1"}
	7880.1.1 = {add_trait=poor_warrior}
	7882.1.1 = {add_spouse=43955901}
}
43955901 = {
	name="Jhigi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7864.1.1 = {birth="7864.1.1"}
}
14055901 = {
	name="Iggi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13855901
	mother=43855901

	7881.1.1 = {birth="7881.1.1"}
}
14155901 = {
	name="Fogo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13955901
	mother=43955901

	7892.1.1 = {birth="7892.1.1"}
	7908.1.1 = {add_trait=poor_warrior}
}
14255901 = {
	name="Haggo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13955901
	mother=43955901

	7895.1.1 = {birth="7895.1.1"}
	7911.1.1 = {add_trait=trained_warrior}
}
14355901 = {
	name="Mirri"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13955901
	mother=43955901

	7896.1.1 = {birth="7896.1.1"}
}
14455901 = {
	name="Temmo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13955901
	mother=43955901

	7899.1.1 = {birth="7899.1.1"}
	7915.1.1 = {add_trait=poor_warrior}
}
