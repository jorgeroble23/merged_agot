####  House Owyang
10069208 = {
	name="Jalil"	# a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	6443.1.1 = {birth="6443.1.1"}
	6459.1.1 = {add_trait=poor_warrior}
	6461.1.1 = {add_spouse=40069208}
	6501.1.1 = {death="6501.1.1"}
}
40069208 = {
	name="Shogofa"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6443.1.1 = {birth="6443.1.1"}
	6501.1.1 = {death="6501.1.1"}
}
10169208 = {
	name="Sajida"	# not a lord
	female=yes
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=10069208
	mother=40069208

	6465.1.1 = {birth="6465.1.1"}
	6516.1.1 = {death="6516.1.1"}
}
10269208 = {
	name="Habiba"	# not a lord
	female=yes
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=10069208
	mother=40069208

	6467.1.1 = {birth="6467.1.1"}
	6522.1.1 = {death="6522.1.1"}
}
10369208 = {
	name="Jalil"	# a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=10069208
	mother=40069208

	6468.1.1 = {birth="6468.1.1"}
	6484.1.1 = {add_trait=trained_warrior}
	6486.1.1 = {add_spouse=40369208}
	6506.1.1 = {death="6506.1.1"}
}
40369208 = {
	name="Habiba"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6468.1.1 = {birth="6468.1.1"}
	6506.1.1 = {death="6506.1.1"}
}
10469208 = {
	name="Fadil"	# a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=10369208
	mother=40369208

	6488.1.1 = {birth="6488.1.1"}
	6504.1.1 = {add_trait=trained_warrior}
	6506.1.1 = {add_spouse=40469208}
	6546.1.1 = {death="6546.1.1"}
}
40469208 = {
	name="Taliba"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6488.1.1 = {birth="6488.1.1"}
	6546.1.1 = {death="6546.1.1"}
}
10569208 = {
	name="Husam"	# not a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=10369208
	mother=40369208

	6490.1.1 = {birth="6490.1.1"}
	6506.1.1 = {add_trait=poor_warrior}
	6536.1.1 = {death="6536.1.1"}
}
10669208 = {
	name="Saghar"	# not a lord
	female=yes
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=10369208
	mother=40369208

	6493.1.1 = {birth="6493.1.1"}
	6570.1.1 = {death="6570.1.1"}
}
10769208 = {
	name="Mansur"	# a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=10469208
	mother=40469208

	6511.1.1 = {birth="6511.1.1"}
	6527.1.1 = {add_trait=trained_warrior}
	6529.1.1 = {add_spouse=40769208}
	6568.1.1 = {death="6568.1.1"}
}
40769208 = {
	name="Asiya"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6511.1.1 = {birth="6511.1.1"}
	6568.1.1 = {death="6568.1.1"}
}
10869208 = {
	name="Zeyd"	# a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=10769208
	mother=40769208

	6545.1.1 = {birth="6545.1.1"}
	6561.1.1 = {add_trait=skilled_warrior}
	6563.1.1 = {add_spouse=40869208}
	6595.1.1 = {death="6595.1.1"}
}
40869208 = {
	name="Taliba"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6545.1.1 = {birth="6545.1.1"}
	6595.1.1 = {death="6595.1.1"}
}
10969208 = {
	name="Khalil"	# not a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=10769208
	mother=40769208

	6547.1.1 = {birth="6547.1.1"}
	6563.1.1 = {add_trait=poor_warrior}
	6608.1.1 = {death="6608.1.1"}
}
11069208 = {
	name="Mansur"	# a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=10869208
	mother=40869208

	6563.1.1 = {birth="6563.1.1"}
	6579.1.1 = {add_trait=poor_warrior}
	6581.1.1 = {add_spouse=41069208}
	6597.1.1 = {death="6597.1.1"}
}
41069208 = {
	name="Habiba"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6563.1.1 = {birth="6563.1.1"}
	6597.1.1 = {death="6597.1.1"}
}
11169208 = {
	name="Gafur"	# a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=11069208
	mother=41069208

	6586.1.1 = {birth="6586.1.1"}
	6602.1.1 = {add_trait=poor_warrior}
	6604.1.1 = {add_spouse=41169208}
	6629.1.1 = {death="6629.1.1"}
}
41169208 = {
	name="Layla"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6586.1.1 = {birth="6586.1.1"}
	6629.1.1 = {death="6629.1.1"}
}
11269208 = {
	name="Shola"	# not a lord
	female=yes
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=11069208
	mother=41069208

	6589.1.1 = {birth="6589.1.1"}
	6638.1.1 = {death="6638.1.1"}
}
11369208 = {
	name="Mahdi"	# not a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=11069208
	mother=41069208

	6590.1.1 = {birth="6590.1.1"}
	6606.1.1 = {add_trait=poor_warrior}
	6649.1.1 = {death="6649.1.1"}
}
11469208 = {
	name="Qamara"	# not a lord
	female=yes
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=11069208
	mother=41069208

	6591.1.1 = {birth="6591.1.1"}
	6648.1.1 = {death="6648.1.1"}
}
11569208 = {
	name="Mahdi"	# a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=11169208
	mother=41169208

	6611.1.1 = {birth="6611.1.1"}
	6627.1.1 = {add_trait=trained_warrior}
	6629.1.1 = {add_spouse=41569208}
	6663.1.1 = {death="6663.1.1"}
}
41569208 = {
	name="Adila"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6611.1.1 = {birth="6611.1.1"}
	6663.1.1 = {death="6663.1.1"}
}
11669208 = {
	name="Aarif"	# not a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=11169208
	mother=41169208

	6613.1.1 = {birth="6613.1.1"}
	6629.1.1 = {add_trait=trained_warrior}
	6654.1.1 = {death="6654.1.1"}
}
11769208 = {
	name="Ali"	# not a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=11169208
	mother=41169208

	6614.1.1 = {birth="6614.1.1"}
	6630.1.1 = {add_trait=trained_warrior}
	6676.1.1 = {death="6676.1.1"}
}
11869208 = {
	name="Azam"	# a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=11569208
	mother=41569208

	6638.1.1 = {birth="6638.1.1"}
	6654.1.1 = {add_trait=poor_warrior}
	6656.1.1 = {add_spouse=41869208}
	6686.1.1 = {death="6686.1.1"}
}
41869208 = {
	name="Sabba"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6638.1.1 = {birth="6638.1.1"}
	6686.1.1 = {death="6686.1.1"}
}
11969208 = {
	name="Setara"	# not a lord
	female=yes
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=11569208
	mother=41569208

	6639.1.1 = {birth="6639.1.1"}
	6703.1.1 = {death="6703.1.1"}
}
12069208 = {
	name="Ramadan"	# a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=11869208
	mother=41869208

	6665.1.1 = {birth="6665.1.1"}
	6681.1.1 = {add_trait=skilled_warrior}
	6683.1.1 = {add_spouse=42069208}
	6707.1.1 = {death="6707.1.1"}
}
42069208 = {
	name="Setara"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6665.1.1 = {birth="6665.1.1"}
	6707.1.1 = {death="6707.1.1"}
}
12169208 = {
	name="Rafiqa"	# not a lord
	female=yes
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=12069208
	mother=42069208

	6685.1.1 = {birth="6685.1.1"}
	6723.1.1 = {death="6723.1.1"}
}
12269208 = {
	name="Husam"	# a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=12069208
	mother=42069208

	6686.1.1 = {birth="6686.1.1"}
	6702.1.1 = {add_trait=master_warrior}
	6704.1.1 = {add_spouse=42269208}
	6740.1.1 = {death="6740.1.1"}
}
42269208 = {
	name="Shameem"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6686.1.1 = {birth="6686.1.1"}
	6740.1.1 = {death="6740.1.1"}
}
12369208 = {
	name="Maryam"	# not a lord
	female=yes
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=12069208
	mother=42069208

	6689.1.1 = {birth="6689.1.1"}
	6739.1.1 = {death="6739.1.1"}
}
12469208 = {
	name="Uways"	# a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=12269208
	mother=42269208

	6718.1.1 = {birth="6718.1.1"}
	6734.1.1 = {add_trait=skilled_warrior}
	6736.1.1 = {add_spouse=42469208}
	6770.1.1 = {death="6770.1.1"}
}
42469208 = {
	name="Layla"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6718.1.1 = {birth="6718.1.1"}
	6770.1.1 = {death="6770.1.1"}
}
12569208 = {
	name="Talib"	# not a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=12269208
	mother=42269208

	6719.1.1 = {birth="6719.1.1"}
	6735.1.1 = {add_trait=poor_warrior}
	6771.1.1 = {death="6771.1.1"}
}
12669208 = {
	name="Aghlab"	# not a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=12269208
	mother=42269208

	6721.1.1 = {birth="6721.1.1"}
	6737.1.1 = {add_trait=poor_warrior}
	6760.1.1 = {death="6760.1.1"}
}
12769208 = {
	name="Halil"	# a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=12469208
	mother=42469208

	6747.1.1 = {birth="6747.1.1"}
	6763.1.1 = {add_trait=trained_warrior}
	6765.1.1 = {add_spouse=42769208}
}
42769208 = {
	name="Sheeva"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6747.1.1 = {birth="6747.1.1"}
}
12869208 = {
	name="Rafiqa"	# not a lord
	female=yes
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=12469208
	mother=42469208

	6749.1.1 = {birth="6749.1.1"}
}
12969208 = {
	name="Yahya"	# not a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=12469208
	mother=42469208

	6752.1.1 = {birth="6752.1.1"}
	6768.1.1 = {add_trait=poor_warrior}
}
13069208 = {
	name="Saghar"	# not a lord
	female=yes
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=12769208
	mother=42769208

	6770.1.1 = {birth="6770.1.1"}
}
13169208 = {
	name="Ubayd"	# not a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=12769208
	mother=42769208

	6771.1.1 = {birth="6771.1.1"}
	6787.1.1 = {add_trait=trained_warrior}
}


####  House Jeung
10069206 = {
	name="Qawurd"	# a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	6441.1.1 = {birth="6441.1.1"}
	6457.1.1 = {add_trait=poor_warrior}
	6459.1.1 = {add_spouse=40069206}
	6499.1.1 = {death="6499.1.1"}
}
40069206 = {
	name="Nyawela"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6441.1.1 = {birth="6441.1.1"}
	6499.1.1 = {death="6499.1.1"}
}
10169206 = {
	name="Sholah"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=10069206
	mother=40069206

	6469.1.1 = {birth="6469.1.1"}
	6497.1.1 = {death="6497.1.1"}
}
10269206 = {
	name="Amsha"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=10069206
	mother=40069206

	6471.1.1 = {birth="6471.1.1"}
	6541.1.1 = {death = {death_reason = death_murder}}
}
10369206 = {
	name="Jahaira"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=10069206
	mother=40069206

	6472.1.1 = {birth="6472.1.1"}
	6482.1.1 = {death="6482.1.1"}
}
10469206 = {
	name="Mahdi"	# a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=10069206
	mother=40069206

	6474.1.1 = {birth="6474.1.1"}
	6490.1.1 = {add_trait=skilled_warrior}
	6492.1.1 = {add_spouse=40469206}
	6532.1.1 = {death="6532.1.1"}
}
40469206 = {
	name="Shameem"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6474.1.1 = {birth="6474.1.1"}
	6532.1.1 = {death="6532.1.1"}
}
10569206 = {
	name="Nasr"	# a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=10469206
	mother=40469206

	6502.1.1 = {birth="6502.1.1"}
	6518.1.1 = {add_trait=skilled_warrior}
	6520.1.1 = {add_spouse=40569206}
	6557.1.1 = {death="6557.1.1"}
}
40569206 = {
	name="Nyawela"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6502.1.1 = {birth="6502.1.1"}
	6557.1.1 = {death="6557.1.1"}
}
10669206 = {
	name="Wahab"	# not a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=10469206
	mother=40469206

	6503.1.1 = {birth="6503.1.1"}
	6519.1.1 = {add_trait=poor_warrior}
	6572.1.1 = {death="6572.1.1"}
}
10769206 = {
	name="Semeah"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=10569206
	mother=40569206

	6520.1.1 = {birth="6520.1.1"}
	6556.1.1 = {death = {death_reason = death_accident}}
}
10869206 = {
	name="Paymaneh"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=10569206
	mother=40569206

	6522.1.1 = {birth="6522.1.1"}
	6564.1.1 = {death="6564.1.1"}
}
10969206 = {
	name="Paymaneh"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=10569206
	mother=40569206

	6523.1.1 = {birth="6523.1.1"}
	6557.1.1 = {death="6557.1.1"}
}
11069206 = {
	name="Is'mail"	# a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=10569206
	mother=40569206

	6526.1.1 = {birth="6526.1.1"}
	6542.1.1 = {add_trait=trained_warrior}
	6544.1.1 = {add_spouse=41069206}
	6581.1.1 = {death="6581.1.1"}
}
41069206 = {
	name="Kamala"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6526.1.1 = {birth="6526.1.1"}
	6581.1.1 = {death="6581.1.1"}
}
11169206 = {
	name="Nizam"	# a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=11069206
	mother=41069206

	6559.1.1 = {birth="6559.1.1"}
	6575.1.1 = {add_trait=poor_warrior}
	6577.1.1 = {add_spouse=41169206}
	6604.1.1 = {death="6604.1.1"}
}
41169206 = {
	name="Amsha"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6559.1.1 = {birth="6559.1.1"}
	6604.1.1 = {death="6604.1.1"}
}
11269206 = {
	name="Parween"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=11069206
	mother=41069206

	6560.1.1 = {birth="6560.1.1"}
	6636.1.1 = {death="6636.1.1"}
}
11369206 = {
	name="Bakr"	# a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=11169206
	mother=41169206

	6583.1.1 = {birth="6583.1.1"}
	6599.1.1 = {add_trait=poor_warrior}
	6601.1.1 = {add_spouse=41369206}
	6630.1.1 = {death="6630.1.1"}
}
41369206 = {
	name="Taneen"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6583.1.1 = {birth="6583.1.1"}
	6630.1.1 = {death="6630.1.1"}
}
11469206 = {
	name="Jalil"	# not a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=11169206
	mother=41169206

	6584.1.1 = {birth="6584.1.1"}
	6600.1.1 = {add_trait=skilled_warrior}
	6617.1.1 = {death = {death_reason = death_battle}}
}
11569206 = {
	name="Nizam"	# not a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=11169206
	mother=41169206

	6585.1.1 = {birth="6585.1.1"}
	6601.1.1 = {add_trait=trained_warrior}
	6663.1.1 = {death="6663.1.1"}
}
11669206 = {
	name="Ghalib"	# a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=11369206
	mother=41369206

	6604.1.1 = {birth="6604.1.1"}
	6620.1.1 = {add_trait=trained_warrior}
	6622.1.1 = {add_spouse=41669206}
	6671.1.1 = {death="6671.1.1"}
}
41669206 = {
	name="Sheeva"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6604.1.1 = {birth="6604.1.1"}
	6671.1.1 = {death="6671.1.1"}
}
11769206 = {
	name="Samira"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=11369206
	mother=41369206

	6606.1.1 = {birth="6606.1.1"}
	6662.1.1 = {death="6662.1.1"}
}
11869206 = {
	name="Sajida"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=11369206
	mother=41369206

	6609.1.1 = {birth="6609.1.1"}
	6656.1.1 = {death="6656.1.1"}
}
11969206 = {
	name="Sadiq"	# not a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=11669206
	mother=41669206

	6623.1.1 = {birth="6623.1.1"}
	6639.1.1 = {add_trait=trained_warrior}
	6645.1.1 = {death="6645.1.1"}
}
12069206 = {
	name="Aram"	# a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=11669206
	mother=41669206

	6624.1.1 = {birth="6624.1.1"}
	6640.1.1 = {add_trait=skilled_warrior}
	6642.1.1 = {add_spouse=42069206}
	6675.1.1 = {death = {death_reason = death_accident}}
}
42069206 = {
	name="Sholah"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6624.1.1 = {birth="6624.1.1"}
	6675.1.1 = {death="6675.1.1"}
}
12169206 = {
	name="Aram"	# a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12069206
	mother=42069206

	6650.1.1 = {birth="6650.1.1"}
	6666.1.1 = {add_trait=poor_warrior}
	6668.1.1 = {add_spouse=42169206}
	6713.1.1 = {death="6713.1.1"}
}
42169206 = {
	name="Rasa"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6650.1.1 = {birth="6650.1.1"}
	6713.1.1 = {death="6713.1.1"}
}
12269206 = {
	name="Sajida"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12069206
	mother=42069206

	6652.1.1 = {birth="6652.1.1"}
	6695.1.1 = {death="6695.1.1"}
}
12369206 = {
	name="Mubarak"	# not a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12069206
	mother=42069206

	6655.1.1 = {birth="6655.1.1"}
	6671.1.1 = {add_trait=trained_warrior}
	6714.1.1 = {death="6714.1.1"}
}
12469206 = {
	name="Yusuf"	# a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12169206
	mother=42169206

	6668.1.1 = {birth="6668.1.1"}
	6684.1.1 = {add_trait=poor_warrior}
	6686.1.1 = {add_spouse=42469206}
	6737.1.1 = {death="6737.1.1"}
}
42469206 = {
	name="Sabba"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6668.1.1 = {birth="6668.1.1"}
	6737.1.1 = {death="6737.1.1"}
}
12569206 = {
	name="Semeah"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12169206
	mother=42169206

	6670.1.1 = {birth="6670.1.1"}
	6745.1.1 = {death="6745.1.1"}
}
12669206 = {
	name="Adila"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12169206
	mother=42169206

	6673.1.1 = {birth="6673.1.1"}
	6697.1.1 = {death="6697.1.1"}
}
12769206 = {
	name="Nasr"	# a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12469206
	mother=42469206

	6694.1.1 = {birth="6694.1.1"}
	6710.1.1 = {add_trait=poor_warrior}
	6712.1.1 = {add_spouse=42769206}
	6772.1.1 = {death = {death_reason = death_murder}}
}
42769206 = {
	name="Shogofa"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6694.1.1 = {birth="6694.1.1"}
	6772.1.1 = {death="6772.1.1"}
}
12869206 = {
	name="Yasmin"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12469206
	mother=42469206

	6697.1.1 = {birth="6697.1.1"}
	6768.1.1 = {death="6768.1.1"}
}
12969206 = {
	name="Qawurd"	# a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12769206
	mother=42769206

	6725.1.1 = {birth="6725.1.1"}
	6741.1.1 = {add_trait=poor_warrior}
	6743.1.1 = {add_spouse=42969206}
}
42969206 = {
	name="Semeah"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6725.1.1 = {birth="6725.1.1"}
}
13069206 = {
	name="Fadl"	# not a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12769206
	mother=42769206

	6727.1.1 = {birth="6727.1.1"}
	6743.1.1 = {add_trait=skilled_warrior}
}
13169206 = {
	name="Reshawna"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12769206
	mother=42769206

	6730.1.1 = {birth="6730.1.1"}
}
13269206 = {
	name="Mirza"	# not a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12769206
	mother=42769206

	6733.1.1 = {birth="6733.1.1"}
	6749.1.1 = {add_trait=skilled_warrior}
	6770.1.1 = {death="6770.1.1"}
}
13369206 = {
	name="Taneen"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12969206
	mother=42969206

	6744.1.1 = {birth="6744.1.1"}
}
13469206 = {
	name="Azam"	# not a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12969206
	mother=42969206

	6745.1.1 = {birth="6745.1.1"}
	6761.1.1 = {add_trait=poor_warrior}
	6763.1.1 = {add_spouse=43469206}
}
43469206 = {
	name="Parand"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6745.1.1 = {birth="6745.1.1"}
}
13569206 = {
	name="Fadil"	# not a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=13469206
	mother=43469206

	6763.1.1 = {birth="6763.1.1"}
	6779.1.1 = {add_trait=poor_warrior}
}


####  House Hawang
10069207 = {
	name="Akin"	# a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	6447.1.1 = {birth="6447.1.1"}
	6463.1.1 = {add_trait=poor_warrior}
	6465.1.1 = {add_spouse=40069207}
	6515.1.1 = {death="6515.1.1"}
}
40069207 = {
	name="Layla"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6447.1.1 = {birth="6447.1.1"}
	6515.1.1 = {death="6515.1.1"}
}
10169207 = {
	name="Habiba"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=10069207
	mother=40069207

	6469.1.1 = {birth="6469.1.1"}
	6522.1.1 = {death="6522.1.1"}
}
10269207 = {
	name="Khalil"	# a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=10069207
	mother=40069207

	6470.1.1 = {birth="6470.1.1"}
	6486.1.1 = {add_trait=skilled_warrior}
	6488.1.1 = {add_spouse=40269207}
	6529.1.1 = {death="6529.1.1"}
}
40269207 = {
	name="Paymaneh"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6470.1.1 = {birth="6470.1.1"}
	6529.1.1 = {death="6529.1.1"}
}
10369207 = {
	name="Saaman"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=10069207
	mother=40069207

	6472.1.1 = {birth="6472.1.1"}
	6494.1.1 = {death="6494.1.1"}
}
10469207 = {
	name="Parween"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=10069207
	mother=40069207

	6473.1.1 = {birth="6473.1.1"}
	6512.1.1 = {death="6512.1.1"}
}
10569207 = {
	name="Yagana"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=10269207
	mother=40269207

	6503.1.1 = {birth="6503.1.1"}
	6576.1.1 = {death="6576.1.1"}
}
10669207 = {
	name="Yakta"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=10269207
	mother=40269207

	6506.1.1 = {birth="6506.1.1"}
	6583.1.1 = {death="6583.1.1"}
}
10769207 = {
	name="Idris"	# a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=10269207
	mother=40269207

	6509.1.1 = {birth="6509.1.1"}
	6525.1.1 = {add_trait=poor_warrior}
	6527.1.1 = {add_spouse=40769207}
	6545.1.1 = {death="6545.1.1"}
}
40769207 = {
	name="Qamara"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6509.1.1 = {birth="6509.1.1"}
	6545.1.1 = {death="6545.1.1"}
}
10869207 = {
	name="Shogofa"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=10269207
	mother=40269207

	6510.1.1 = {birth="6510.1.1"}
	6546.1.1 = {death="6546.1.1"}
}
10969207 = {
	name="Zeyd"	# a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=10769207
	mother=40769207

	6530.1.1 = {birth="6530.1.1"}
	6546.1.1 = {add_trait=skilled_warrior}
	6548.1.1 = {add_spouse=40969207}
	6586.1.1 = {death="6586.1.1"}
}
40969207 = {
	name="Sajida"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6530.1.1 = {birth="6530.1.1"}
	6586.1.1 = {death="6586.1.1"}
}
11069207 = {
	name="Asiya"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=10969207
	mother=40969207

	6550.1.1 = {birth="6550.1.1"}
	6591.1.1 = {death="6591.1.1"}
}
11169207 = {
	name="Sholah"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=10969207
	mother=40969207

	6553.1.1 = {birth="6553.1.1"}
	6597.1.1 = {death="6597.1.1"}
}
11269207 = {
	name="Rashida"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=10969207
	mother=40969207

	6554.1.1 = {birth="6554.1.1"}
	6612.1.1 = {death="6612.1.1"}
}
11369207 = {
	name="Akin"	# a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=10969207
	mother=40969207

	6555.1.1 = {birth="6555.1.1"}
	6571.1.1 = {add_trait=skilled_warrior}
	6573.1.1 = {add_spouse=41369207}
	6612.1.1 = {death="6612.1.1"}
}
41369207 = {
	name="Parand"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6555.1.1 = {birth="6555.1.1"}
	6612.1.1 = {death="6612.1.1"}
}
11469207 = {
	name="Fadl"	# a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=11369207
	mother=41369207

	6579.1.1 = {birth="6579.1.1"}
	6595.1.1 = {add_trait=skilled_warrior}
	6597.1.1 = {add_spouse=41469207}
	6633.1.1 = {death="6633.1.1"}
}
41469207 = {
	name="Taneen"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6579.1.1 = {birth="6579.1.1"}
	6633.1.1 = {death="6633.1.1"}
}
11569207 = {
	name="Abdul"	# not a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=11369207
	mother=41369207

	6580.1.1 = {birth="6580.1.1"}
	6596.1.1 = {add_trait=skilled_warrior}
	6655.1.1 = {death = {death_reason = death_accident}}
}
11669207 = {
	name="Youkhanna"	# a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=11469207
	mother=41469207

	6597.1.1 = {birth="6597.1.1"}
	6613.1.1 = {add_trait=poor_warrior}
	6615.1.1 = {add_spouse=41669207}
	6646.1.1 = {death="6646.1.1"}
}
41669207 = {
	name="Habiba"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6597.1.1 = {birth="6597.1.1"}
	6646.1.1 = {death="6646.1.1"}
}
11769207 = {
	name="Sheeftah"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=11669207
	mother=41669207

	6619.1.1 = {birth="6619.1.1"}
	6684.1.1 = {death="6684.1.1"}
}
11869207 = {
	name="Saaman"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=11669207
	mother=41669207

	6620.1.1 = {birth="6620.1.1"}
	6621.1.1 = {death="6621.1.1"}
}
11969207 = {
	name="Mirza"	# a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=11669207
	mother=41669207

	6621.1.1 = {birth="6621.1.1"}
	6637.1.1 = {add_trait=poor_warrior}
	6639.1.1 = {add_spouse=41969207}
	6667.1.1 = {death="6667.1.1"}
}
41969207 = {
	name="Qamara"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6621.1.1 = {birth="6621.1.1"}
	6667.1.1 = {death="6667.1.1"}
}
12069207 = {
	name="Zeyd"	# a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=11969207
	mother=41969207

	6639.1.1 = {birth="6639.1.1"}
	6655.1.1 = {add_trait=master_warrior}
	6657.1.1 = {add_spouse=42069207}
	6698.1.1 = {death="6698.1.1"}
}
42069207 = {
	name="Adila"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6639.1.1 = {birth="6639.1.1"}
	6698.1.1 = {death="6698.1.1"}
}
12169207 = {
	name="Ghalib"	# a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=12069207
	mother=42069207

	6660.1.1 = {birth="6660.1.1"}
	6676.1.1 = {add_trait=skilled_warrior}
	6678.1.1 = {add_spouse=42169207}
	6722.1.1 = {death="6722.1.1"}
}
42169207 = {
	name="Semeah"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6660.1.1 = {birth="6660.1.1"}
	6722.1.1 = {death="6722.1.1"}
}
12269207 = {
	name="Nafisa"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=12069207
	mother=42069207

	6662.1.1 = {birth="6662.1.1"}
	6702.1.1 = {death="6702.1.1"}
}
12369207 = {
	name="Nyawela"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=12069207
	mother=42069207

	6663.1.1 = {birth="6663.1.1"}
	6692.1.1 = {death="6692.1.1"}
}
12469207 = {
	name="Is'mail"	# not a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=12069207
	mother=42069207

	6666.1.1 = {birth="6666.1.1"}
	6682.1.1 = {add_trait=trained_warrior}
	6705.1.1 = {death = {death_reason = death_battle}}
}
12569207 = {
	name="Shamir"	# a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=12169207
	mother=42169207

	6683.1.1 = {birth="6683.1.1"}
	6699.1.1 = {add_trait=poor_warrior}
	6701.1.1 = {add_spouse=42569207}
	6739.1.1 = {death="6739.1.1"}
}
42569207 = {
	name="Kamala"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6683.1.1 = {birth="6683.1.1"}
	6739.1.1 = {death="6739.1.1"}
}
12669207 = {
	name="Saaman"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=12569207
	mother=42569207

	6705.1.1 = {birth="6705.1.1"}
	6753.1.1 = {death="6753.1.1"}
}
12769207 = {
	name="Taliba"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=12569207
	mother=42569207

	6707.1.1 = {birth="6707.1.1"}
	6746.1.1 = {death="6746.1.1"}
}
12869207 = {
	name="Semeah"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=12569207
	mother=42569207

	6710.1.1 = {birth="6710.1.1"}
	6715.1.1 = {death="6715.1.1"}
}
12969207 = {
	name="Yahya"	# a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=12569207
	mother=42569207

	6712.1.1 = {birth="6712.1.1"}
	6728.1.1 = {add_trait=poor_warrior}
	6730.1.1 = {add_spouse=42969207}
}
42969207 = {
	name="Rafiqa"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6712.1.1 = {birth="6712.1.1"}
}
13069207 = {
	name="Ali"	# not a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=12969207
	mother=42969207

	6745.1.1 = {birth="6745.1.1"}
	6761.1.1 = {add_trait=poor_warrior}
	6763.1.1 = {add_spouse=43069207}
}
43069207 = {
	name="Shararah"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	6745.1.1 = {birth="6745.1.1"}
}
13169207 = {
	name="Semeah"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=12969207
	mother=42969207

	6747.1.1 = {birth="6747.1.1"}
}
13269207 = {
	name="Azam"	# not a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=12969207
	mother=42969207

	6750.1.1 = {birth="6750.1.1"}
	6766.1.1 = {add_trait=poor_warrior}
}
13369207 = {
	name="Sami"	# not a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=13069207
	mother=43069207

	6773.1.1 = {birth="6773.1.1"}
	6789.1.1 = {add_trait=poor_warrior}
}
13469207 = {
	name="Tanaz"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=13069207
	mother=43069207

	6775.1.1 = {birth="6775.1.1"}
}
13569207 = {
	name="Nyawela"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=13069207
	mother=43069207

	6776.1.1 = {birth="6776.1.1"}
}
