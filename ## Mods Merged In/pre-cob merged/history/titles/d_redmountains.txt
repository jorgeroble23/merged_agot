2478.1.1={
	liege="k_torrentineTK"
	law = succ_primogeniture
	law = cognatic_succession
	effect = { #cull spawned western valyrians
		holder_scope = {
			any_realm_character = {
				limit = {
					culture_group = valyrian
					NOT = { father_even_if_dead = { always = yes } }
					NOT = { mother_even_if_dead = { always = yes } }
					OR = {
						dynasty = 0
						tier = BARON
					}
					NOT = { has_character_flag = high_valyrian }
					NOT = { has_dynasty_flag = high_valyrian }
					high_valyrian_dynasty_trigger = no
				}
				culture = old_first_man
				set_graphical_culture = old_first_man
			}
		}
	}
}
4751.1.1 = {
	holder = 150016 #Samwell
}
4798.1.1 = {
	holder = 0
}

6413.1.1 = { holder=60016 } # Arthur (nc)
6455.1.1 = { holder=24017 } # Gerold (nc)
6479.2.5= {
holder=23017
}
6532.1.1= {
holder=22017
}
6554.1.1= {
holder=21017
}
6576.1.1= {
holder=20017
}
6590.1.1= {
holder=18017
}
6610.1.1= {
holder=17017
}

6636.8.4= {
	holder=15017
}
6637.3.5= {
holder=13017
}

6682.1.1= {
holder=12017
}
6709.1.1= {
holder=11017
}
6743.1.1= {
holder=1017
}
6750.1.1= {
holder=1016
}
6762.1.1= {
holder=6016
}
6774.1.1= {
holder=9016
}
