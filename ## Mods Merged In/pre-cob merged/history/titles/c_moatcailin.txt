6468.1.1 = { 
	holder = 1050126 #Last Marsh king
	liege = k_theneckTK
}
6506.1.1={
	holder = 814059 #Rickard
	liege="e_north"
	law = succ_appointment
	effect = {
		set_title_flag = military_command
		holder_scope = {
			if = {
				limit = { primary_title = { title = PREVPREV } }
				set_government_type = military_command_government 
				PREV = { succession = appointment }
				recalc_succession = yes
			}
		}
	}	
}

6531.1.1 = {
	holder = 8321059 #Ellard
}
6563.1.1 = {
	holder = 8331059 #Brandon
}
6597.1.1 = {
	holder = 820059 #Theon the Hungry Wolf
}
6638.1.1 = {
	holder = 831059 #Eddard
}
6663.1.1 = {
	holder = 8119059 #Brandon
}
6672.1.1={
	holder = 8118059 #Brandon
}
6738.1.1={
	holder = 817059 #Jonnel
}
6743.1.1={
	holder = 816059 #Dorren
}
6793.1.1 = {
	holder = 850059 #Brandon
}

6801.1.1 = {
	holder = 8150059 #Brandon
}
6820.1.1 = {
	holder = 813059 #Rodrik
}
