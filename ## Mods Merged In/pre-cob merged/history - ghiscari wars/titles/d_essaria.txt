400.1.1={
	#liege="k_sarnor"
	effect = {
		holder_scope = { 
			k_sarnor = {
				holder_scope = {
					make_tributary = { who = PREVPREV tributary_type = sarnori }
				}
			}
		}	
	}
	law = succ_primogeniture
}

2992.1.1 = { holder=10056803 } # Burhan (nc)
3021.1.1 = { holder=10156803 } # Sahat (nc)
3056.1.1 = { holder=10556803 } # Ekavir (nc)
3093.1.1 = { holder=10656803 } # Shyam (nc)
3104.1.1 = { holder=11256803 } # Taresh (nc)
3133.1.1 = { holder=11756803 } # Taresh (nc)
3153.1.1 = { holder=11956803 } # Jaafar (nc)
3164.1.1 = { holder=12156803 } # Yathavan (nc)
3200.1.1 = { holder=12256803 } # Paarth (nc)
3252.1.1 = { holder=12456803 } # Jash (nc)
3278.1.1 = { holder=12856803 } # Bhagesh (nc)