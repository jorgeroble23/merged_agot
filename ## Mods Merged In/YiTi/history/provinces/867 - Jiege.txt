# 867 - Jiege

# County Title
title = c_jiege

# Settlements
max_settlements = 6

b_jiege_castle1 = castle
b_jiege_city1 = city
b_jiege_temple = temple
b_jiege_city2 = city
b_jiege_castle2 = city
b_jiege_castle3 = castle

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_jiege_castle1 = ca_asoiaf_yiti_basevalue_1
	b_jiege_castle1 = ca_asoiaf_yiti_basevalue_2
	b_jiege_castle1 = ca_asoiaf_yiti_basevalue_3

	b_jiege_city1 = ct_asoiaf_yiti_basevalue_1
	b_jiege_city1 = ct_asoiaf_yiti_basevalue_2
	b_jiege_city1 = ct_asoiaf_yiti_basevalue_3

	b_jiege_city2 = ct_asoiaf_yiti_basevalue_1
	b_jiege_city2 = ct_asoiaf_yiti_basevalue_2
	b_jiege_city2 = ct_asoiaf_yiti_basevalue_3

}
	