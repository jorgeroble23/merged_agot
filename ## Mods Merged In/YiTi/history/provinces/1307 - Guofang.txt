# 1210  - Guofang

# County Title
title = c_guofang

# Settlements
max_settlements = 4

b_guofang_castle = castle
b_guofang_city1 = city
b_guofang_temple = temple
b_guofang_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_guofang_castle = ca_asoiaf_yiti_basevalue_1
	b_guofang_castle = ca_asoiaf_yiti_basevalue_2

	b_guofang_city1 = ct_asoiaf_yiti_basevalue_1
	b_guofang_city1 = ct_asoiaf_yiti_basevalue_2

}
	