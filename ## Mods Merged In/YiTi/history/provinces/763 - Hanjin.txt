# 763  - Hanjin

# County Title
title = c_hanjin

# Settlements
max_settlements = 6

b_hanjin_castle1 = castle
b_hanjin_city1 = city
b_hanjin_temple = temple
b_hanjin_city2 = city
b_hanjin_castle2 = city
b_hanjin_castle3 = castle

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_hanjin_castle1 = ca_asoiaf_yiti_basevalue_1
	b_hanjin_castle1 = ca_asoiaf_yiti_basevalue_2
	b_hanjin_castle1 = ca_asoiaf_yiti_basevalue_3

	b_hanjin_city1 = ct_asoiaf_yiti_basevalue_1
	b_hanjin_city1 = ct_asoiaf_yiti_basevalue_2
	b_hanjin_city1 = ct_asoiaf_yiti_basevalue_3

	b_hanjin_city2 = ct_asoiaf_yiti_basevalue_1
	b_hanjin_city2 = ct_asoiaf_yiti_basevalue_2
	b_hanjin_city2 = ct_asoiaf_yiti_basevalue_3

}
	