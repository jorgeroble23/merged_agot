# 869  - Wusu

# County Title
title = c_wusu

# Settlements
max_settlements = 6

b_wusu_castle1 = castle
b_wusu_city1 = city
b_wusu_temple = temple
b_wusu_city2 = city
b_wusu_castle2 = city
b_wusu_castle3 = castle

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_wusu_castle1 = ca_asoiaf_yiti_basevalue_1
	b_wusu_castle1 = ca_asoiaf_yiti_basevalue_2
	b_wusu_castle1 = ca_asoiaf_yiti_basevalue_3

	b_wusu_city1 = ct_asoiaf_yiti_basevalue_1
	b_wusu_city1 = ct_asoiaf_yiti_basevalue_2
	b_wusu_city1 = ct_asoiaf_yiti_basevalue_3

	b_wusu_city2 = ct_asoiaf_yiti_basevalue_1
	b_wusu_city2 = ct_asoiaf_yiti_basevalue_2
	b_wusu_city2 = ct_asoiaf_yiti_basevalue_3

}
	